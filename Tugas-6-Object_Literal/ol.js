// ******************** Tugas 6 - Object Literal **********************

// SOAL 1
console.log("======================= SOAL 1 ==========================");
var now = new Date();
var thisYear = now.getFullYear(); // 2020 (tahun sekarang)

function arrayToObject(arr) {
  for (var i = 0; i < arr.length; i++) {
    var umur = 0;
    var no = i + 1;
    if (arr[i][3] == null || arr[i][3] > thisYear) {
      umur = "Invalid Birth Year";
    } else {
      umur = thisYear - arr[i][3];
    }

    var arrObject = {
      firstName: arr[i][0],
      lastName: arr[i][1],
      gender: arr[i][2],
      age: umur,
    };
    console.log(
      no + ". " + arrObject.firstName + " " + arrObject.lastName + ": "
    );
    console.log(arrObject);
  }
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
console.log();
var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
arrayToObject([]);

// SOAL 2
console.log("\n======================= SOAL 2 ==========================");

// fungsi untuk mengurutkan DESC
function compare(a, b) {
  const satu = a.harga;
  const dua = b.harga;

  let comparison = 0;
  if (satu < dua) {
    comparison = 1;
  } else if (satu > dua) {
    comparison = -1;
  }
  return comparison;
}

function shoppingTime(memberId, money) {
  // list produk
  var listProduct = [
    { produk: "Sepatu brand Stacattu", harga: 1500000 },
    { produk: "Baju brand Zoro", harga: 500000 },
    { produk: "Baju brand H&N", harga: 250000 },
    { produk: "Casing Handphone", harga: 50000 },
    { produk: "Sweater brand Uniklooh", harga: 175000 },
  ];

  if (memberId == null || memberId == "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else {
    if (money < 50000) {
      return "Mohon maaf, uang tidak cukup";
    } else {
      var tempListpurchased = []; // list produk sementara
      var productDesc = listProduct.sort(compare); // sorting produk termahal ke termurah (DESC)
      var tempMoney = money; // total uang sementara

      // menambahkan produk yg harganya sesuai uang dimiliki dan mengurangi uang dgn harga produk yg dipilih
      for (var i = 0; i < productDesc.length; i++) {
        if (productDesc[i].harga <= tempMoney) {
          tempListpurchased.push(productDesc[i].produk);
          tempMoney = tempMoney - productDesc[i].harga;
        }
      }

      // set array
      var printOut = {
        memberId: memberId,
        money: money,
        listPurchased: tempListpurchased,
        changeMoney: tempMoney,
      };

      return printOut;
    }
  }
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());

// SOAL 3
console.log("\n======================= SOAL 3 ==========================");

function naikAngkot(arrPenumpang) {
  var rute = ["A", "B", "C", "D", "E", "F"];
  var printOut = [];
  for (var i = 0; i < arrPenumpang.length; i++) {
    for (var j = 0; j < rute.length; j++) {
      var awal;
      var akhir;
      if (rute[j] == arrPenumpang[i][1]) {
        awal = j;
      }
      if (rute[j] == arrPenumpang[i][2]) {
        akhir = j;
      }
    }

    var TotalBayar = 2000 * (akhir - awal);
    var Obj = {
      penumpang: arrPenumpang[i][0],
      naikDari: arrPenumpang[i][1],
      tujuan: arrPenumpang[i][2],
      bayar: TotalBayar,
    };
    printOut.push(Obj);
  }
  return printOut;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
