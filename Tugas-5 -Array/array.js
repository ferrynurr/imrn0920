/******** Tugas 5 - Array **********/

// SOAL 1
	console.log('--------------- SOAL 1 ------------------');
	function range(startNum=0, finishNum=0)
	{
		var number = [];
		if(startNum > 0 && finishNum > 0)
		{
			if(startNum < finishNum)
			{
				for(let i = startNum; i <= finishNum; i++)
					number.push(i);
			}
			else if(startNum > finishNum)
			{
				for(let i = startNum; i >= finishNum; i--)
					number.push(i);
			}
		}else{
			number.unshift(-1);
		}
		
		return number;
		
	}
	console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	console.log(range(1)) // -1
	console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
	console.log(range(54, 50)) // [54, 53, 52, 51, 50]	
	console.log(range()) // -1 
	
	
// SOAL 2
	console.log('\n---------------- SOAL 2 -----------------');
	function rangeWithStep(startNum=0, finishNum=0, step =1)
	{
		var RangeStep = [];
		if(startNum > 0 || finishNum > 0)
		{
			if(startNum < finishNum)
			{
				for(let i = startNum; i <= finishNum; i++)
				{
					
					RangeStep.push(i);
					i+=step-1;
					
				}
			}
			else if(startNum > finishNum)
			{
				for(let i = startNum; i >= finishNum; i--)
				{
					RangeStep.push(i);
					i-=step-1;
				}
			}
		}else{
			RangeStep.unshift(0);
		}
		return RangeStep;
		
	}
	console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
	console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
	console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
	console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
	
	
// SOAL 3
	console.log('\n---------------- SOAL 3 -----------------');
	function sum(awal, akhir, jarak)
	{
		var getDeret = rangeWithStep(awal, akhir, jarak);
		var total =  0;
		for(let i=0; i < getDeret.length; i++)
		{    
			total += getDeret[i];		  
		}
		
		return total;
	}
	
	console.log(sum(1,10)) // 55
	console.log(sum(5, 50, 2)) // 621
	console.log(sum(15,10)) // 75
	console.log(sum(20, 10, 2)) // 90
	console.log(sum(1)) // 1
	console.log(sum()) // 0 


// SOAL 4
	console.log('\n---------------- SOAL 4 -----------------');
	var input = [
					["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
					["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
					["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
					["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
				];
	
	function dataHandling (arrayInput)
	{
		for (let i = 0; i < arrayInput.length; i++) 
		{
	
			console.log('Nomor ID: '+arrayInput[i][0]);
			console.log('Nama Lengkap: '+arrayInput[i][1]);
			console.log('TTL: '+arrayInput[i][2]+' '+arrayInput[i][3]);
			console.log('Hobi: '+arrayInput[i][4]);
			console.log();
			
		}
		
	}
	dataHandling(input);


// SOAL 5
	console.log('\n---------------- SOAL 5 -----------------');
	function balikKata(kata) 
	{
		var KataSekarang = kata;
		var KataTerbalik = '';
		
		for (let i = kata.length - 1; i >= 0; i--) {
		  KataTerbalik = KataTerbalik + KataSekarang[i];
		 }
	 
		return KataTerbalik;
	}
	
	console.log(balikKata("Kasur Rusak")) // kasuR rusaK
	console.log(balikKata("SanberCode")) // edoCrebnaS
	console.log(balikKata("Haji Ijah")) // hajI ijaH
	console.log(balikKata("racecar")) // racecar
	console.log(balikKata("I am Sanbers")) // srebnaS ma I 
	
	
// SOAL 6
	console.log('\n---------------- SOAL 6 -----------------');
	var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
	dataHandling2(input);
	
	function dataHandling2(inputArray)
	{
		inputArray.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
		inputArray.splice(4, 1, "Pria", "SMA Internasional Metro");
		console.log(inputArray);
		
		var pisah = inputArray[3].split("/");
		var bulan_string;
		switch(pisah[1]) 
		{
		  case '01' :   
				  bulan_string = 'Januari'; 
				  break;
		  case '02' :   
				  bulan_string = 'Februari'; 
				  break;
		  case '03' :   
				  bulan_string = 'Maret';
				  break; 
		  case '04' :   
				  bulan_string = 'April'; 
				  break; 
		  case '05' :   
				  bulan_string = 'Mei'; 
				  break; 
		  case '06' :   
				  bulan_string = 'Juni';
				  break; 
		  case '07' :   
				  bulan_string = 'Juli'; 
				  break; 
		  case '08' :   
				  bulan_string = 'Agustus';
				  break; 
		  case '09' :   
				  bulan_string = 'September'; 
				  break; 
	      case '10' :  
				  bulan_string = 'Oktober'; 
				  break; 
          case '11' :  
				  bulan_string = 'November'; 
				  break; 
		  case '12' :  
				  bulan_string = 'Desember'; 
				  break; 
		  default :  
				  bulan_string = '-';
		}
		console.log(bulan_string);
		
		var sortingTgl = pisah.sort(function (value1, value2) { return value2 - value1 } ) ;
		console.log(sortingTgl);
		
		var gantiPemisah = inputArray[3].split("/").join("-");
		console.log(gantiPemisah);
		
		var limitNama = inputArray[1].slice(0,14) ;
		console.log(limitNama);
	}