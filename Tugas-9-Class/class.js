/* ****************** TUGAS 9 - CLASS ********************* */

// SOAL1
console.log("================ SOAL 1 ===============");
class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }

  get getName() {
    return this.name;
  }

  get getLegs() {
    return this.legs;
  }

  get getCold_blooded() {
    return this.cold_blooded;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.getName); // "shaun"
console.log(sheep.getLegs); // 4
console.log(sheep.getCold_blooded); // false

class Ape extends Animal {
  constructor(name) {
    super(name);
  }

  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }

  jump() {
    console.log("hop hop");
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

// SOAL 2
console.log("\n================ SOAL 2 ===============");
class Clock {
  constructor({ template }) {
    this.template = template;
    this.timer;
  }
  render() {
    var date = new Date();
    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;
    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;
    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;
    var output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }
  stop() {
    clearInterval(timer);
  }
  start() {
    this.timer = setInterval(this.render.bind(this), 1000);
  }
}
var clock = new Clock({ template: "h:m:s" });
clock.start();
