//	******** Tugas 4 Functions  **********

// SOAL 1
	function teriak() 
	{
	  return 'Halo Sanbers!';
	}
	console.log(teriak());
	console.log('');
	
// SOAL 2
	function kalikan(x, y)
	{
		 return x * y;
	}
	
	var num1 = 12;
	var num2 = 4; 
	var hasilKali = kalikan(num1, num2);
	console.log(hasilKali);
	console.log('');
	
// SOAL 3
	function introduce(a, b, c, d)
	{
		var data = 'Nama saya '+ a +', umur saya '+ b +' tahun, alamat saya di '+ c +', dan saya punya hobby yaitu '+ d +'!';
		
		return data;
	}
	
	var name = "Agus";
	var age = 30;
	var address = "Jln. Malioboro, Yogyakarta";
	var hobby = "Gaming";
	 
	var perkenalan = introduce(name, age, address, hobby);
	console.log(perkenalan);