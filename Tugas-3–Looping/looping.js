// ******** Tugas 3 – Looping ********

// SOAL 1

	var deret = 10;
	var jumlah = 0;

	console.log('LOOPING PERTAMA');
	while(deret > 0) {
	  jumlah += 2;
	  deret --; 
	  console.log(jumlah + ' - I love coding');
	}

	var deret2 = 10;
	var jumlah2 = 20;

	console.log('LOOPING KEDUA');
	while(deret2 < 20) {
	   deret2 ++; 
	   console.log(jumlah2 + ' - I will become a mobile developer');
	   jumlah2 -= 2; 
	}
	console.log('');

// SOAL 2
    var i =1;
	for(var angka = i; angka <= 20; angka++) 
	{
	    if (angka % 2 == 0) {
		  console.log( angka + ' - Berkualitas');
		}
		else if((angka % 2 != 0) && (angka % 3 == 0))
		{
			console.log( angka + ' - I Love Coding');
		}
		else {
		  console.log( angka + ' - Santai');
		}
	} 
	console.log('');

// SOAL 3
	var k = 1;
	while(k <= 4) 
	{
		for (var j=1;j<=8;j++) {
			process.stdout.write('#');
		}
		console.log('');
		
	  k++;
	 
	}
	console.log('');
		
// SOAL 4
    for (var x=1; x <= 7; x++) {
        for (var l=1; l <= x; l++) {
            process.stdout.write('#');
        }
        console.log('');
    }
	console.log('');

// SOAL 5
	var p=1;
	while(p <= 8) 
	{
		if( p % 2 == 0)
		{
			for(var h = 1; h <= 8; h++)
			{
				if (h % 2 == 0) {
				   process.stdout.write(' ');
				}
				else {
				  process.stdout.write('#');
				}
			}
		}
		else
		{
			for(var h = 1; h <= 8; h++)
			{
				if (h % 2 != 0) {
				   process.stdout.write(' ');
				}
				else {
				  process.stdout.write('#');
				}
			}
		
		}
		
		console.log('');
		p++;
	} 
	console.log('');
