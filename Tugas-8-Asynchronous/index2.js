var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise

function Baca() {
  readBooksPromise(10000, books[0])
    .then(function (fulfilled) {
      return readBooksPromise(fulfilled, books[1]);
    })
    .then(function (fulfilled2) {
      return readBooksPromise(fulfilled2, books[2]);
    })
    .catch(function (error) {
      return error.message;
    });
}

Baca();
