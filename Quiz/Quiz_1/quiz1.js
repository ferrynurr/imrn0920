/***************   QUIZ 1 - ESSAI   **************/


// SOAL :

// 13. Palindrome (10 poin). Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. Function akan me-return tipe data boolean: true jika string merupakan palindrome, dan false jika string bukan palindrome. NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . Hanya boleh gunakan looping. // TEST CASES Palindrome console.log(palindrome("kasur rusak")) // true console.log(palindrome("haji ijah")) // true console.log(palindrome("nabasan")) // false console.log(palindrome("nababan")) // true console.log(palindrome("jakarta")) // false
// 14. Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst. NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . Hanya boleh gunakan looping. // TEST CASES BalikString console.log(balikString("abcde")) // edcba console.log(balikString("rusak")) // kasur console.log(balikString("racecar")) // racecar console.log(balikString("haji")) // ijah
// 15. 15. Descending Ten (15 poin). Function DescendingTen adalah kebalikan dari function AscendingTen. Output yang diharapkan adalah deretan angka dimulai dari angka parameter hingga 10 angka di bawahnya. Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan. contoh: console.log(DescendingTen(10)) akan menampilkan 10 9 8 7 6 5 4 3 2 1 console.log(DescendingTen(20)) akan menampilkan 20 19 18 17 16 15 14 13 12 11 Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number. // TEST CASES Descending Ten console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91 console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1 console.log(DescendingTen()) // -1
// 16. Papan Ular Tangga (20). Buatlah sebuah function ularTangga yang ketika function tersebut dipanggil akan menampilkan papan ular tangga ukuran 10 x 10. Output: 100 99 98 97 96 95 94 93 92 91 81 82 83 84 85 86 87 88 89 90 80 79 78 77 76 75 74 73 72 71 61 62 63 64 65 66 67 68 69 70 60 59 58 57 56 55 54 53 52 51 41 42 43 44 45 46 47 48 49 50 40 39 38 37 36 35 34 33 32 31 21 22 23 24 25 26 27 28 29 30 20 19 18 17 16 15 14 13 12 11 1 2 3 4 5 6 7 8 9 10 */ // TEST CASE Ular Tangga console.log(ularTangga())


// JAWABAN :

// NO. 13
function palindrome(kata) 
{ 
    var KataSekarang = kata;
     var KataTerbalik = []; 
     for (let i = kata.length - 1; i >= 0; i--)
      { 
          KataTerbalik = KataTerbalik + KataSekarang[i];
      } 
     if (KataTerbalik == KataSekarang) 
        return true; 
     else 
        return false; 
} 
console.log(palindrome("kasur rusak")); //true 
console.log(palindrome("haji ijah")); // true 
console.log(palindrome("nabasan")); // false 
console.log(palindrome("nababan")); // true 
console.log(palindrome("jakarta")); // false

// NO.14
function balikString(kata) 
{ 
    var KataSekarang = kata;
     var KataTerbalik = ""; 
     for (let i = kata.length - 1; i >= 0; i--) 
     { 
         KataTerbalik = KataTerbalik + KataSekarang[i];
    } 
        return KataTerbalik; 
} 
console.log(balikString("abcde")); // edcba
console.log(balikString("rusak")); // kasur 
console.log(balikString("racecar")); // racecar 
console.log(balikString("haji")); // ijah

// NO.15
function DescendingTen(startNum) 
{ 
    var tampung = []; 
    if (startNum > 0) 
    {
        var bantu = startNum - 9; 
        for (let i = startNum; i >= bantu; i--) 
        { 
             tampung.push(i); 
        } 
    } else 
    { 
        tampung.push(-1);
    } 
    var toString = tampung.join(" ");
    return toString; 
} 
console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91 
console.log(DescendingTen(10)); // 10 9 8 7 6 5 4 3 2 1 
console.log(DescendingTen()); // -1

// NO.16
function ularTangga() {
  var deret = ""
  for (var i = 10; i >= 1; i--){
    var flag = 10 * i
    if (i%2 == 0) {
      for (var y = flag; y >= flag-9; y--)
      deret += y+" "
    } else {
      for (var y = flag-9; y <= flag; y++)
      deret += y+" "
    }
    
    
    deret +="\n"
  }

  return deret
}

console.log(ularTangga());
