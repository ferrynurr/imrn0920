import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SignIn, CreateAccount, Profile, about, Skill, Search, Add, Project, Search2, Splash} from './components/Screens';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import { Ionicons } from '@expo/vector-icons';
import { AuthContext } from './context';

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator headerMode="none">
    
    <AuthStack.Screen name="CreateAccount" component={CreateAccount} />
    <AuthStack.Screen name="SignIn" component={SignIn} headerMode="none" />
  </AuthStack.Navigator>
)
const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator 
   screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'md-home' : 'md-home';
            } else if (route.name === 'Card') {
              iconName = focused ? 'ios-basket' : 'ios-basket';
            } else if (route.name === 'Message') {
              iconName = focused ? 'ios-mail' : 'ios-mail';
            
            } else if (route.name === 'Profile') {
              iconName = focused ? 'ios-person' : 'ios-person';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#F77866',
          inactiveTintColor: 'gray',
        }}>
   
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="Card" component={AddStackScreen} />
    <Tabs.Screen name="Message" component={ProjectStackScreen} />
    <Tabs.Screen name="Profile" component={AboutStackScreen} />
  </Tabs.Navigator>
)

const AddStack = createStackNavigator();
const AddStackScreen = () => (
  <AddStack.Navigator headerMode="none">
    <AddStack.Screen name="Add" component={Add} />
  </AddStack.Navigator>
)

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
  <ProjectStack.Navigator headerMode="none">
    <ProjectStack.Screen name="Project" component={Project} />
  </ProjectStack.Navigator>
)
const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
  <AboutStack.Navigator headerMode="none">
    <AboutStack.Screen name="About" component={about} />
  </AboutStack.Navigator>
)

const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator headerMode="none">
    <HomeStack.Screen name="Skill" component={Skill} />
  </HomeStack.Navigator>
)

const SearchStack = createStackNavigator();
const SearchStackScreen = () => (
  <SearchStack.Navigator headerMode="none">
    <SearchStack.Screen name="Search" component={Search} />
    <SearchStack.Screen name="Search2" component={Search2} />
  </SearchStack.Navigator>
)

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => (
  <ProfileStack.Navigator headerMode="none">
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home" >
    <Drawer.Screen name="Home" component={TabsScreen} />
    <Drawer.Screen name="Profile" component={ProfileStackScreen} />
  </Drawer.Navigator>
)

const RootStack = createStackNavigator()
const RootStackScreen = ( {userToken} ) => (
  <RootStack.Navigator headerMode="none">
    {userToken ? (
      <RootStack.Screen name="Auth" component={DrawerScreen} />
    ) : (
      <RootStack.Screen name="App" component={AuthStackScreen} />
    )}
  </RootStack.Navigator>
)

export default function App() {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false)
        setUserToken("pitong")
      },
      signUp: () => {
        setIsLoading(false)
        setUserToken("pitong")
      },
      signOut: () => {
        setIsLoading(false)
        setUserToken(null)
      },
    }
  }, [])

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, [])

  if (isLoading){
    return <Splash />
  }
  return(
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
    
  );
}
