import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StatusBar,
  TextInput,
  ScrollView
} from 'react-native';


import { Entypo } from '@expo/vector-icons'; 
import { FontAwesome } from '@expo/vector-icons';   
import flashsale from '../flashsale.json';
import produk from '../produk.json';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Ionicons } from '@expo/vector-icons'; 
import { FontAwesome5 } from '@expo/vector-icons'; 
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

const Item = ({ item }) => (
  <TouchableOpacity  style={styles.item}>
       <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{width: 60, height: 50,}}>
            <Icon style={{ alignSelf: "center"}} name={item.iconName} size={50} />
        </View>
        <View style={{width: 120, height: 50}}>
            <Text style={styles.skillName}>{item.skillName}</Text>
            <Text style={styles.percentageProgress}>{item.percentageProgress}</Text>
        </View>
      
      </View>
  </TouchableOpacity>
);

const SkillScreen = () => {
  const renderItem = ({ item }) => {
    return (
      <Item
        item={item}
        style='#B4E9FF' 
      />
    );
  };

    return (
    
    <View style={styles.container}> 
    <ScrollView>
        <View style={styles.header}>
          <Text style={styles.headerText}></Text>
        </View> 
         <View style={styles.content}>
              <View style={styles.tentang}>
                    <View style={{width: '85%', height: 50, }}>
                        <TextInput placeholder="Cari sesuatu..."
                            style={ styles.inputText }
                        />
                    </View>
                    <View style={{width: '15%', height: 50, justifyContent: 'center', alignItems: 'center' }} >
                       <Ionicons name="ios-notifications" size={24} color="#F77866" />
                         
                    </View>
                    
              </View>
               <View style={styles.logo}>
                <Image source={require('../images/banner.jpg')} style={{ width: '100%', height: 200 }} />
              </View>
                <View style={styles.tentang}>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                            <MaterialCommunityIcons name="tshirt-crew" size={24} color="white" />
                            
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Man</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                            <MaterialCommunityIcons name="tshirt-crew" size={24} color="white" />
                            
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Woman</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                            <FontAwesome5 name="baby" size={24} color="white" />
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Kids</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                            <FontAwesome5 name="home" size={24} color="white" />
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Home</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={[styles.katagori, { backgroundColor: 'black'}]}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                           <Entypo name="chevron-thin-right" size={24} color="white" />
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>More</Text>
                    </View>

                </View>
        <Text style={{marginTop: 20, fontWeight: 'bold', fontSize:17}}>Flash Sale</Text>
      
        <FlatList  horizontal={true}
            data={flashsale.items}
            renderItem={renderItem}
            keyExtractor = {(item) => `key-${item.id}`}
        />
        <Text style={{marginTop: 20, fontWeight: 'bold', fontSize:17}}>New Product</Text>
        <FlatList  horizontal={true}
            data={produk.items}
            renderItem={renderItem}
            keyExtractor = {(item) => `key-${item.id}`}
        />
         </View> 
       </ScrollView>
    </View>
    );
  
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
         backgroundColor: '#fff',
  },

  col: {
    
    backgroundColor: "grey",
    borderWidth: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,

    paddingTop: 10,
 
    borderColor: '#EFEFEF',
    backgroundColor: '#EFEFEF',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    width: '100%',
    marginTop:21,
  
  },
    logo : {
    flexDirection: 'column',
    alignItems: 'flex-end',
    marginTop:20
  },
  header: {
    backgroundColor: '#F77866',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 0,
   borderBottomColor: '#ddd',
  },
  headerText: {
    padding: 8,
  },

  content: {
    margin:15
  },
  tentang: {
        flexDirection: 'row',
        marginTop:20
  },
  skillHead:{
    marginTop:30
  },
  border:{
     width: '100%', 
     height: 5, 
     backgroundColor: '#003366',
     marginBottom:10
  },
  katagori: {
       height: 50, 
       backgroundColor: '#F77866',   
       borderBottomLeftRadius: 10,
       borderBottomRightRadius: 10,
       borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      marginRight:5,
      justifyContent: 'center',
      alignItems: 'center',
      
     
  },
  container2: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#B4E9FF',
    padding:8,
    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    fontSize: 32,
  },
  skillName: {
      fontSize:20,
      fontWeight:'bold',
  },
  categoryName: {
      fontSize:15,
      fontWeight:'bold',
      color: '#19d3da',
      
  },
  percentageProgress: {
      fontSize:35,
      fontWeight:'bold',
      color: '#FFF',
      alignSelf: "flex-end"
  },
  inputText: {
    borderBottomWidth:2,
    width: '100%',
    height: 40,
    marginBottom: 20,
    borderBottomColor: '#F77866',
    color: 'black',
    fontSize: 20,
    paddingLeft: 8,
    paddingRight: 8
  },
});

export default SkillScreen;