import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableHighlight,
  TextInput,
} from 'react-native';

import { AntDesign } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { SimpleLineIcons } from '@expo/vector-icons'; 
import { AuthContext } from "../context";
const RegisterScreen = () => {
 const { signIn } = React.useContext(AuthContext);
    return (
    <ScrollView>
        <View style={styles.container}>
           
            <Text style={styles.headerText}>Welcome</Text>
            <Text style={{color: 'black', paddingBottom:20}}>Sign up to Continue</Text>
              <View style={styles.form}>
                  <Text style={styles.titleText}>Nama</Text>
                  <TextInput placeholder="Masukan Nama..."
                      style={ styles.inputText }
                  />
                  <Text style={styles.titleText}>Email</Text>
                  <TextInput placeholder="Masukan Alamat Email..."
                      style={ styles.inputText }
                  />
                  <Text style={styles.titleText}>Phone Number</Text>
                  <TextInput placeholder="Masukan Nomor Telepon..."
                      style={ styles.inputText }
                  />
                  <Text style={styles.titleText}>Password</Text>
                  <TextInput placeholder="Masukan Password..."
                      style={ styles.inputText } secureTextEntry={true}
                  />
   
                
                  <View style={{alignItems: 'center', paddingTop:10}}>
                      <TouchableHighlight
                          style={styles.submit}
                          onPress={() => signIn()}
                          underlayColor='#fff'>
                          <Text style={[styles.submitText, styles.btnDaftar]}>Sign Up </Text>
                      </TouchableHighlight>
                      <View style={{flex: 1, flexDirection: 'row',  textAlign:'center'}}>
                        <View style={{width: 150, height: 50}}>
                           <Text style={{color: 'black', fontSize: 14, alignSelf: "flex-end"}}>Already have an account? </Text>
                        </View>
                        <View style={{width: 80, height: 50}}>
                             <Text style={{color:'#F77866', fontSize: 14}} onPress={() => signIn()}>Sign in</Text>
                        </View>
                      </View>

                     
                     
                      

                  </View>
              </View>

        </View>
    </ScrollView>
    );
  
}
export default RegisterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,

   justifyContent: 'center',
    marginTop: 40
  },
  logo : {
    justifyContent: 'center',
  },
  form : {
    padding: 20,
     backgroundColor: '#fff',
  },
  inputText: {
   // borderWidth:2,
   borderBottomWidth:2,
    width: 300,
    height: 40,
    marginBottom: 20,
   borderColor: '#E6EAEE',
    color: 'black',
    fontSize: 18,
    //paddingLeft: 8,
    paddingRight: 8
  },
  titleText :{
    fontSize: 18,
    fontWeight: 'normal',
    color: '#4D4D4D'
  },
  headerText  : {
    paddingTop: 40,
    paddingBottom:5,
    fontSize: 40,
    fontWeight: 'bold',
    color: 'black'
  },

  submit:{
    width: '100%',
    borderWidth: 1,
    borderColor: '#fff',
    marginBottom: 10,
    marginTop: 10
  },

  submitText:{
    color:'#fff',
    textAlign:'center',
    fontWeight: 'bold',
    fontSize: 20,
  },

  btnDaftar : {
    paddingTop:15,
    paddingBottom:15,
    borderRadius:20,
    backgroundColor:'#F77866'
  },

  btnLogin: {
    paddingTop:15,
    paddingBottom:15,
    borderRadius:20,
    backgroundColor:'#3EC6FF'
      
  }
});