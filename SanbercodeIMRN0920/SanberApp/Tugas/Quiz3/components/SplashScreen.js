import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image

} from 'react-native';

const SplashScreen = () => {
    return (
    <View style={styles.container}>   
          <View style={styles.logo}>
            <Image source={require('../images/sanber.jpg')} style={{ width: 180, height: 180 }} />
          </View>
    </View>
    );
  
}
export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  logo: {
        padding: 80,
        backgroundColor: 'grey',
        borderColor: 'grey',
        borderRadius: 180
  }




});