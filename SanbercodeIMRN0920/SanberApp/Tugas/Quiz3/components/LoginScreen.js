
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  FlatList,
  TextInput,
  Button
} from 'react-native';

import { AntDesign } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 
import { AuthContext } from "../context";
const LoginScreen = () => {
 const { signIn } = React.useContext(AuthContext);
    return (
      <View style={styles.container}>
          <View style={styles.logo}>
            <Image source={require('../images/logo.png')} style={{ width: 370, height: 100 }} />
          </View>
          <Text style={styles.headerText}>Login</Text>
          <View style={styles.form}>
            <Text style={styles.titleText}>Username / Email</Text>
            <TextInput placeholder="Masukan Username/email..."
                style={ styles.inputText }
            />
            
            <Text style={styles.titleText}>Password</Text>
            <TextInput placeholder="Masukan Password..."
                style={ styles.inputText } secureTextEntry={true}
            />
            <View style={{alignItems: 'center'}}>
                <TouchableHighlight
                    style={styles.submit}
                    onPress={() => {
                        alert('Login Berhasil');
                    }}
                    underlayColor='#fff'>
                    <Text style={[styles.submitText, styles.btnLogin]}  onPress={() => signIn()}> Masuk <AntDesign name="login" size={24} color="white" /></Text>
                </TouchableHighlight>

                <Text style={{color: '#3EC6FF', fontSize: 20}}>atau</Text>

                <TouchableHighlight
                    style={styles.submit}

                    underlayColor='#fff'>
                    <Text style={[styles.submitText, styles.btnDaftar]}>Daftar <Entypo name="add-user" size={24} color="white" /> </Text>
                </TouchableHighlight>
             </View>
          </View>

      </View>
    );
  
}
export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo : {
    justifyContent: 'center',
  },
  form : {
    paddingTop: 40
  },
  inputText: {
    borderWidth:2,
    width: 300,
    height: 40,
    marginBottom: 20,
    borderColor: '#003366',
    color: 'black',
    fontSize: 20,
    paddingLeft: 8,
    paddingRight: 8
  },
  titleText :{
    fontSize: 18,
    fontWeight: 'normal',
    color: '#003366'
  },
  headerText  : {
    paddingTop: 80,
    fontSize: 28,
    fontWeight: 'bold',
    color: '#003366'
  },

  submit:{
    width: 200,
    borderWidth: 1,
    borderColor: '#fff',
    marginBottom: 10,
    marginTop: 10
  },

  submitText:{
    color:'#fff',
    textAlign:'center',
    fontWeight: 'bold',
    fontSize: 20,
  },

  btnDaftar : {
    paddingTop:15,
    paddingBottom:15,
    borderRadius:20,
    backgroundColor:'#003366'
  },

  btnLogin: {
    paddingTop:15,
    paddingBottom:15,
    borderRadius:20,
    backgroundColor:'#3EC6FF'
      
  }
});

// export default function LoginScreen() {
//   const { signIn } = React.useContext(AuthContext);
//   return (
//     <View style={styles.container}>
//       <Image source={require('../images/logo.png')} style={styles.logoImage}/>
//       <View style={styles.loginContainer}>
//         <Text style={styles.activityTitle}>Login</Text>
//             <Text style={styles.titleText}>Username / Email</Text>
//             <TextInput placeholder="Masukan Username/email..."
//                 style={ styles.inputText }
//             />
            
//             <Text style={styles.titleText}>Password</Text>
//             <TextInput placeholder="Masukan Password..."
//                 style={ styles.inputText } secureTextEntry={true}
//             />
//         <TouchableOpacity style={styles.btnLogin} onPress={() => signIn()} >
//           <Text style={{color: '#ffff'}} >Masuk</Text>
//         </TouchableOpacity>
//         <Text style={{marginTop: 15, color: '#3EC6FF'}}>atau</Text>
//         <TouchableOpacity style={styles.btnDaftar}>
//           <Text style={{color: '#ffff'}}>Daftar</Text>
//         </TouchableOpacity>
//       </View>
      
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//    backgroundColor: '#ffff',
//   },
//   logoImage: {
//     marginTop: 55,
//   },
//   activityTitle: {
//     fontSize: 24,
//     color: '#003366',
    
//   },
//   loginContainer: {
//     flex: 1,
//     alignItems: 'center',
//   },
//   textInput: { 
//     marginTop: 15
//   },

//   loginTextInput: { 
//     height: 40, 
//     width: 280,
//     borderColor: '#003366', 
//     borderWidth: 1,
//   },
//     inputText: {
//     borderWidth:2,
//     width: 300,
//     height: 40,
//     marginBottom: 20,
//     borderColor: '#003366',
//     color: 'black',
//     fontSize: 20,
//     paddingLeft: 8,
//     paddingRight: 8
//   },
//   titleText :{
//     fontSize: 18,
//     fontWeight: 'normal',
//     color: '#003366'
//   },
//   btnLogin: {
//     height: 40, 
//     width: 100,
//     borderRadius: 25,
//     marginTop: 15,
//     alignItems: 'center',
//     justifyContent: 'center',
//     backgroundColor: '#3EC6FF'
//   },
//   btnDaftar: {
//     height: 40, 
//     width: 100,
//     borderRadius: 25,
//     marginTop: 15,
//     alignItems: 'center',
//     justifyContent: 'center',
//     backgroundColor: '#003366'
//   }
// });
