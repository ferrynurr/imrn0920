import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SignIn, CreateAccount, Profile, Skill, Search, Add, Project, Search2, Splash} from './components/Screens';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import { Ionicons } from '@expo/vector-icons';
import { AuthContext } from './context';

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator headerMode="none">
    <AuthStack.Screen name="SignIn" component={SignIn} headerMode="none" />
    <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{title: 'Create Account'}} />
  </AuthStack.Navigator>
)
const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator 
   screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Skill') {
              iconName = focused ? 'ios-book' : 'ios-book';
            } else if (route.name === 'Add') {
              iconName = focused ? 'ios-add-circle' : 'ios-add-circle-outline';
            } else if (route.name === 'Project') {
              iconName = focused ? 'ios-cube' : 'ios-cube';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#003366',
          inactiveTintColor: 'gray',
        }}>
   
    <Tabs.Screen name="Skill" component={HomeStackScreen} />
    <Tabs.Screen name="Add" component={AddStackScreen} />
    <Tabs.Screen name="Project" component={ProjectStackScreen} />
  </Tabs.Navigator>
)

const AddStack = createStackNavigator();
const AddStackScreen = () => (
  <AddStack.Navigator headerMode="none">
    <AddStack.Screen name="Add" component={Add} />
  </AddStack.Navigator>
)

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
  <ProjectStack.Navigator headerMode="none">
    <ProjectStack.Screen name="Project" component={Project} />
  </ProjectStack.Navigator>
)

const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator headerMode="none">
    <HomeStack.Screen name="Skill" component={Skill} />
  </HomeStack.Navigator>
)

const SearchStack = createStackNavigator();
const SearchStackScreen = () => (
  <SearchStack.Navigator headerMode="none">
    <SearchStack.Screen name="Search" component={Search} />
    <SearchStack.Screen name="Search2" component={Search2} />
  </SearchStack.Navigator>
)

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => (
  <ProfileStack.Navigator headerMode="none">
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home" >
    <Drawer.Screen name="Home" component={TabsScreen} />
    <Drawer.Screen name="Profile" component={ProfileStackScreen} />
  </Drawer.Navigator>
)

const RootStack = createStackNavigator()
const RootStackScreen = ( {userToken} ) => (
  <RootStack.Navigator headerMode="none">
    {userToken ? (
      <RootStack.Screen name="Auth" component={DrawerScreen} />
    ) : (
      <RootStack.Screen name="App" component={AuthStackScreen} />
    )}
  </RootStack.Navigator>
)

export default function App() {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false)
        setUserToken("pitong")
      },
      signUp: () => {
        setIsLoading(false)
        setUserToken("pitong")
      },
      signOut: () => {
        setIsLoading(false)
        setUserToken(null)
      },
    }
  }, [])

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, [])

  if (isLoading){
    return <Splash />
  }
  return(
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
    
  );
}
