import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StatusBar
} from 'react-native';


import { Entypo } from '@expo/vector-icons'; 
import { FontAwesome } from '@expo/vector-icons';   
import skillData from '../skillData.json';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const Item = ({ item }) => (
  <TouchableOpacity  style={styles.item}>
       <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{width: '30%', height: 80,}}>
            <Icon style={{ alignSelf: "center"}} name={item.iconName} size={75} />
        </View>
        <View style={{width: '50%', height: 80}}>
            <Text style={styles.skillName}>{item.skillName}</Text>
            <Text style={styles.categoryName}>{item.categoryName}</Text>
            <Text style={styles.percentageProgress}>{item.percentageProgress}</Text>
        </View>
        <View style={{width: '20%', height: 80}}>
            <Entypo name="chevron-thin-right" size={70} color="black" />
        </View>
      </View>
  </TouchableOpacity>
);

const SkillScreen = () => {
  const renderItem = ({ item }) => {
    return (
      <Item
        item={item}
        style='#B4E9FF' 
      />
    );
  };

    return (
    
    <View style={styles.container}> 
        <View style={styles.header}>
          <Text style={styles.headerText}></Text>
        </View> 
         <View style={styles.logo}>
            <Image source={require('../images/logo.png')} style={{ width: 170, height: 55 }} />
        </View> 
         <View style={styles.content}>
              <View style={styles.tentang}>
                    <View style={{width: '20%', height: 50, }}>
                        <FontAwesome name="user-circle-o" size={50} color="#B4E9FF" />
                    </View>
                    <View style={{width: '80%', height: 50, justifyContent: 'center'}} >
                         <Text style={{fontSize:18, fontWeight: 'bold'}}>Hai,</Text> 
                         <Text style={{color: '#003366', fontSize:20,  fontWeight: 'bold'}}>M. FERRY NURDIN</Text>
                         
                         
                    </View>
                    
              </View>
               <View style={styles.skillHead}>
                   <Text style={{fontSize:30}}>SKILL</Text> 
                    <View style={styles.border}></View>  
               </View>
                <View style={styles.tentang}>
                    <View style={styles.katagori}>
                        <Text style={{fontSize:12, fontWeight: 'bold'}}>Library/Framework</Text> 
                    </View>
                     <View style={styles.katagori}>
                        <Text style={{fontSize:12, fontWeight: 'bold'}}>Bahasa Pemrograman</Text> 
                    </View>
                     <View style={styles.katagori}>
                        <Text style={{fontSize:12, fontWeight: 'bold'}}>Teknologi</Text> 
                    </View>
                </View>
        </View> 

       
        <FlatList
            data={skillData.items}
            renderItem={renderItem}
            keyExtractor = {(item) => `key-${item.id}`}
        />
       
    </View>
    );
  
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
         backgroundColor: '#fff',
  },

  col: {
    
    backgroundColor: "grey",
    borderWidth: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,

    paddingTop: 10,
 
    borderColor: '#EFEFEF',
    backgroundColor: '#EFEFEF',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    width: '100%',
    marginTop:21,
  
  },
    logo : {
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  header: {
    backgroundColor: '#B4E9FF',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 0,
   borderBottomColor: '#ddd',
  },
  headerText: {
    padding: 10,
  },

  content: {
    margin:15
  },
  tentang: {
        flexDirection: 'row'
  },
  skillHead:{
    marginTop:30
  },
  border:{
     width: '100%', 
     height: 5, 
     backgroundColor: '#003366',
     marginBottom:10
  },
  katagori: {
      width: '32.5%',
       height: 30, 
       backgroundColor: '#B4E9FF',   
       borderBottomLeftRadius: 10,
       borderBottomRightRadius: 10,
       borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      marginRight:5,
      justifyContent: 'center',
      alignItems: 'center',
      
     
  },
  container2: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#B4E9FF',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  skillName: {
      fontSize:20,
      fontWeight:'bold',
  },
  categoryName: {
      fontSize:15,
      fontWeight:'bold',
      color: '#19d3da',
      
  },
  percentageProgress: {
      fontSize:35,
      fontWeight:'bold',
      color: '#FFF',
      alignSelf: "flex-end"
  },


});

export default SkillScreen;