import React from "react";
import { View, Text, StyleSheet, Button, Alert } from "react-native";
import { AuthContext } from "../context";
import { AntDesign } from '@expo/vector-icons'; 
import Login from './LoginScreen'
import ProjectScreen from './ProjectScreen'
import SkillScreen from './SkillScreen'
import AddScreen from './AddScreen'
import About from './AboutScreen'
import { ScrollView } from "react-native-gesture-handler";
import { DrawerItems, SafeAreaView } from "@react-navigation/native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Details = ({ route }) => (
  <ScreenContainer>
    <Text>Details Screen</Text>
    {route.params.name && <Text>{route.params.name}</Text>}
  </ScreenContainer>
);

export const Add = () => (
  <ScreenContainer>
    <AddScreen/>
  </ScreenContainer>
);

export const Project = () => (
  <ScreenContainer>
   <ProjectScreen />
  </ScreenContainer>
);

export const Skill = () => (
  <ScreenContainer>
    <SkillScreen />
  </ScreenContainer>
);

export const Profile = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);

  return (
    <ScrollView>
      <About />
    </ScrollView>
  );
};

export const Splash = () => (
  <ScreenContainer>
    <Text>Loading...</Text>
  </ScreenContainer>
);

export const SignIn = ({ navigation }) => {
  const { signIn } = React.useContext(AuthContext);

  return (
    <Login />
  );
};

export const CreateAccount = () => {
  const { signUp } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
      <Text>Create Account Screen</Text>
      <Button title="Sign Up" onPress={() => signUp()} />
    </ScreenContainer>
  );
};

