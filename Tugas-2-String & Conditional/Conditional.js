// *** B. Tugas Conditional ***
	// If-else
		var nama  = 'Taylor Otwell';
		var peran = 'GuaRD';
		
		var peran_lower = peran.toLowerCase();
			
		if(nama != '' && peran_lower != '')
		{
			console.log('Selamat datang di Dunia Werewolf, '+nama);
			
			if(peran_lower == 'penyihir')
				console.log('Halo '+ peran +' '+ nama +', kamu dapat melihat siapa yang menjadi werewolf!');
			else if(peran_lower == 'guard')
				console.log('Halo '+ peran +' '+ nama +', kamu akan membantu melindungi temanmu dari serangan werewolf.');
			else if(peran_lower == 'werewolf')
				console.log('Halo '+ peran +' '+ nama +', Kamu akan memakan mangsa setiap malam!');
			else
				console.log('Peran Yang kamu pilih Tidak Tersedia');

		}
		else if(nama != '' && peran_lower == '')
			console.log('Halo '+ nama +', Pilih peranmu untuk memulai game!')
		else
			console.log('Nama harus diisi!');
		
		console.log('');
	
	// Switch Case
		var hari  = 31; 
		var bulan = 3; 
		var tahun = 2020;
		
		var hari_value;
		var bulan_string;
		var tahun_value;
		
		switch(true)
		{
			case (hari > 0 && hari <= 31):
				hari_value = hari;
				break;
			default:
				hari_value =  '-';
				break;
		}
		
		switch(bulan) 
		{
		  case 1 :   
				  bulan_string = 'Januari'; 
				  break;
		  case 2 :   
				  bulan_string = 'Februari'; 
				  break;
		  case 3 :   
				  bulan_string = 'Maret';
				  break; 
		  case 4 :   
				  bulan_string = 'April'; 
				  break; 
		  case 5 :   
				  bulan_string = 'Mei'; 
				  break; 
		  case 6 :   
				  bulan_string = 'Juni';
				  break; 
		  case 7 :   
				  bulan_string = 'Juli'; 
				  break; 
		  case 8 :   
				  bulan_string = 'Agustus';
				  break; 
		  case 9 :   
				  bulan_string = 'September'; 
				  break; 
	      case 10 :  
				  bulan_string = 'Oktober'; 
				  break; 
          case 11 :  
				  bulan_string = 'November'; 
				  break; 
		  case 12 :  
				  bulan_string = 'Desember'; 
				  break; 
		  default :  
				  bulan_string = '-';
		}
		
		switch(true)
		{
			case (tahun >= 1900 && tahun <= 2200):
				tahun_value = tahun;
				break;
			default:
				tahun_value =  '-';
				break;
		}
		
		console.log(hari_value +' '+ bulan_string +' '+ tahun_value);
		
		
	